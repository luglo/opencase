<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

class DbTestCase extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seed();

        // debugging purposes
        // $this->withoutExceptionHandling();
    }
}
