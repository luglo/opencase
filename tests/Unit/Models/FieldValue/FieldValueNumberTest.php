<?php

namespace Tests\Unit\Models\FieldValue;

use App\Models\FieldValue\FieldValueNumber;

use Tests\DbTestCase;

class FieldValueNumberTest extends DbTestCase
{
    public function test_floats()
    {
        $number = FieldValueNumber::factory()->create();
        $inputs = [
            -200,
            -40400593.9001274415,
            0,
            1,
            33.3333333333,
            22.2222222221,
            128645756.2000001,
            40400593.900127447812,
            200.00,

        ];
        foreach ($inputs as $input) {
            $number->value = $input;
            $number->save();
            $value = round($input, 8);
            $this->assertEquals($value, $number->value);
        }
    }
}
