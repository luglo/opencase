<?php

namespace Tests\Unit\Models\Relationships;

use App\Models\Entry\Entry;
use App\Models\Entry\EntryType;
use App\Models\Entry\EntryFieldset;

use Tests\DbTestCase;

class EntryRelationshipTest extends DbTestCase
{
    public function test_entry_has_type()
    {
        $entry = Entry::factory()->create();
        $entryType = EntryType::factory()->create();

        $entry->type()->associate($entryType)->save();

        $this->assertEquals($entry->type->id, $entryType->id);
    }

    public function test_entry_has_fieldsets()
    {
        $count = rand(1, 5);
        $entry = Entry::factory()->create();
        $fieldsets = EntryFieldset::factory()->count($count)->create();

        $entry->fieldsets()->saveMany($fieldsets);
        foreach ($fieldsets as $fieldset) {
            $this->assertTrue($entry->fieldsets->contains($fieldset));
        }
        $this->assertEquals($count, $entry->fieldsets->count());
    }
}
