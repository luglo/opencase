<?php

namespace Tests\Unit\Models\Relationships;

use App\Models\Entry\EntryField;
use App\Models\Entry\EntryFieldset;
use App\Models\FieldValue\FieldValueNumber;

use Tests\DbTestCase;

class EntryFieldRelationshipTest extends DbTestCase
{
    public function test_fieldvalue_to_entryfield_relationship()
    {
        $entryField = EntryField::factory()->create();
        $number = FieldValueNumber::factory()->create();

        $number->entryfield()->save($entryField);
        $number->refresh();

        $this->assertTrue($entryField->fieldvalueable->value === $number->value);
        $this->assertTrue($entryField->fieldvalue() === $number->value);
    }

    public function test_entryfield_to_fieldvalue_relationship()
    {
        $entryField = EntryField::factory()->create();
        $number = FieldValueNumber::factory()->create();
        
        $entryField->fieldvalueable()->associate($number);

        $this->assertTrue($entryField->fieldvalueable->value === $number->value);
        $this->assertTrue($entryField->fieldvalue() === $number->value);
    }

    public function test_entryfield_has_fieldset()
    {
        $entryField = EntryField::factory()->create();
        $fieldset = EntryFieldset::factory()->create();

        $entryField->fieldset()->associate($fieldset)->save();

        $this->assertEquals($entryField->fieldset->id, $fieldset->id);
    }
}
