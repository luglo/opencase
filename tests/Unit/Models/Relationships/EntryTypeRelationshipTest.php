<?php

namespace Tests\Unit\Models\Relationships;

use App\Models\Entry\Entry;
use App\Models\Entry\EntryType;

use Tests\DbTestCase;

class EntryTypeRelationshipTest extends DbTestCase
{
    public function test_entrytype_has_entries()
    {
        $count = rand(1, 5);
        $entries = Entry::factory()->count($count)->create();
        $entryType = EntryType::factory()->create();

        $entryType->entries()->saveMany($entries);

        foreach ($entries as $entry) {
            $this->assertTrue($entryType->entries->contains($entry));
        }
        $this->assertEquals($count, $entryType->entries->count());
    }
}
