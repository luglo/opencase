<?php

namespace Tests\Unit\Models\Relationships;

use App\Models\User;
use App\Models\Entry\EntryField;
use App\Models\FieldValue\FieldValueNumber;

use Tests\DbTestCase;

class EntryFieldValueRelationshipTest extends DbTestCase
{
    public function test_fieldvalue_has_entryfield()
    {
        $entryField = EntryField::factory()->create();
        $number = FieldValueNumber::factory()->create();

        $number->entryfield()->save($entryField);
        $number->refresh();

        $this->assertEquals($entryField->id, $number->entryfield->id);
    }

    public function test_fieldvalue_has_revisions_with_user_and_values()
    {
        $number = FieldValueNumber::factory()->create();
        $number->refresh();
        $oldValue = $number->value;
        $inputs = [
            -2147483648, // int min
            -200,
            -40400593.9001274432, // decimals
            0,
            1,
            40400593.9001274432,
            2147483647, // int max
            9223372036854775807 // bigint max
        ];
        foreach ($inputs as $input) {
            $user = User::factory()->create();
            $this->actingAs($user);
            $number->value = $input;
            $number->save();
            $number->refresh();
            $version = $number->revisionHistory->last();
            $this->assertEquals($oldValue, $version->oldValue());
            $this->assertEquals($number->value, $version->newValue());
            $this->assertEquals($user->id, $version->userResponsible()->id);
            $oldValue = $number->value;
        }
    }
}
