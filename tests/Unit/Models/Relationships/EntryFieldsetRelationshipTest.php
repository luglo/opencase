<?php

namespace Tests\Unit\Models\Relationships;

use App\Models\Entry\Entry;
use App\Models\Entry\EntryType;
use App\Models\Entry\EntryField;
use App\Models\Entry\EntryFieldset;

use Tests\DbTestCase;

class EntryFieldsetRelationshipTest extends DbTestCase
{
    public function test_entryfieldset_has_entry()
    {
        $fieldset = EntryFieldset::factory()->create();
        $entry = Entry::factory()->create();

        $fieldset->entry()->associate($entry)->save();
        $this->assertEquals($fieldset->entry->id, $entry->id);
    }

    public function test_entryfieldset_has_types()
    {
        $count = rand(1, 5);
        $fieldset = EntryFieldset::factory()->create();
        $entryTypes = EntryType::factory()->count($count)->create();

        $fieldset->types()->saveMany($entryTypes);

        foreach ($entryTypes as $entryType) {
            $this->assertTrue($fieldset->types->contains($entryType));
        }
        $this->assertEquals($count, $fieldset->types->count());
    }

    public function test_entryfieldset_has_fields()
    {
        $count = rand(1, 5);
        $fieldset = EntryFieldset::factory()->create();
        $fields = EntryField::factory()->count($count)->create();

        $fieldset->fields()->saveMany($fields);

        foreach ($fields as $field) {
            $this->assertTrue($fieldset->fields->contains($field));
        }
        $this->assertEquals($count, $fieldset->fields->count());
    }
}
