<?php

namespace App\Models\Entry;

use Spatie\Translatable\HasTranslations;
use App\Models\BaseModel;
use App\Models\Entry\EntryType;

class Entry extends BaseModel
{
    use HasTranslations;

    protected $translatable = [
        'title',
        'description',
    ];

    protected $fillable = [
        'title',
        'description',
    ];

    public function type()
    {
        return $this->belongsTo(EntryType::class, 'entry_type_id');
    }

    public function fieldsets()
    {
        return $this->hasMany(EntryFieldset::class);
    }
}
