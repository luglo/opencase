<?php

namespace App\Models\Entry;

use App\Models\BaseModel;
use App\Models\Entry\EntryFieldset;
use Spatie\Translatable\HasTranslations;

class EntryField extends BaseModel
{
    use HasTranslations;

    protected $translatable = [
        'label',
        'description',
    ];

    protected $fillable = [
        'label',
        'description'
    ];

    public function fieldvalueable()
    {
        return $this->morphTo();
    }

    public function fieldvalue()
    {
        return $this->fieldvalueable ? $this->fieldvalueable->value : null;
    }

    public function fieldset()
    {
        return $this->belongsTo(EntryFieldset::class, 'entry_fieldset_id');
    }
}
