<?php

namespace App\Models\Entry;

use App\Models\BaseModel;
use App\Models\Entry\Entry;
use App\Models\Entry\EntryFieldset;
use Spatie\Translatable\HasTranslations;

class EntryType extends BaseModel
{
    use HasTranslations;

    protected $translatable = [
        'title',
        'description',
    ];

    protected $fillable = [
        'name',
        'title',
        'description',
    ];

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function fieldsets()
    {
        return $this->belongsToMany(EntryFieldset::class, 'entry_entry_fieldsets');
    }
}