<?php

namespace App\Models\Entry;

use Spatie\Translatable\HasTranslations;
use App\Models\BaseModel;
use App\Models\Entry\Entry;
use App\Models\Entry\EntryField;
use App\Models\Traits\HasEntryType;

class EntryFieldset extends BaseModel
{
    use HasTranslations;
    use HasEntryType;

    protected $translatable = [
        'label',
        'description',
    ];

    protected $fillable = [
        'label',
        'description',
    ];

    public function entry()
    {
        return $this->belongsTo(Entry::class, 'entry_id');
    }

    public function fields()
    {
        return $this->hasMany(EntryField::class);
    }
}
