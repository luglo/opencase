<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;

/**
 * EntryField value of type Text
 */
class FieldValueDatetime extends FieldValue
{
    protected $table = 'field_value_datetimes';

    public function __construct(array $attributes = [])
    {
        $this->casts = array_merge($this->casts, [
            'value' => 'datetime'
        ]);

        parent::__construct($attributes);
    }
}
