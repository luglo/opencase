<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;

/**
 * EntryField value of type Text
 */
class FieldValueDate extends FieldValue
{
    protected $table = 'field_value_dates';

    public function __construct(array $attributes = [])
    {
        $this->casts = array_merge($this->casts, [
            'value' => 'date'
        ]);

        parent::__construct($attributes);
    }
}
