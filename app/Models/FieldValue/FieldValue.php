<?php

namespace App\Models\FieldValue;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\Models\Entry\EntryField;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Base class for all entry field values
 * This should be extended by all field type models
 */
class FieldValue extends Model
{
    use HasFactory; 
    use HasTranslations;
    use RevisionableTrait;
    
    protected $translatable = [
        'description',
    ];

    protected $fillable = [
        'description',
        'value'
    ];

    /**
     * Returns the EntryField relationship
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entryfield()
    {
        return $this->morphOne(EntryField::class, 'fieldvalueable');
    }
    
}
