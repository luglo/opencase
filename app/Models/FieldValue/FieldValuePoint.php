<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * EntryField value of type Text
 */
class FieldValuePoint extends FieldValue
{
    protected $table = 'field_value_points';

    public function __construct(array $attributes = [])
    {
        $this->casts = array_merge($this->casts, [
            'value' => Point::class
        ]);

        parent::__construct($attributes);
    }
}
