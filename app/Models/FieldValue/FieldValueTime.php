<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;

/**
 * EntryField value of type Text
 */
class FieldValueTime extends FieldValue
{
    protected $table = 'field_value_times';

    public function __construct(array $attributes = [])
    {
        $this->casts = array_merge($this->casts, [
            'value' => 'datetime:H:i:s'
        ]);

        parent::__construct($attributes);
    }
}
