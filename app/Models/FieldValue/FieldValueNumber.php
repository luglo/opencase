<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * EntryField value of type Number
 */
class FieldValueNumber extends FieldValue
{
    protected $table = 'field_value_numbers';

    protected function value(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => round($value, 8),
            set: fn ($value) => round($value, 8)
        );
    }    
}
