<?php

namespace App\Models\FieldValue;

use App\Models\FieldValue\FieldValue;

/**
 * EntryField value of type Text
 */
class FieldValueText extends FieldValue
{
    protected $table = 'field_value_texts';

    public function __construct(array $attributes = [])
    {
        $this->translatable = array_merge($this->translatable, [
            'value'
        ]);

        parent::__construct($attributes);
    }
}
