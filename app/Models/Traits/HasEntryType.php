<?php

namespace App\Models\Traits;

use App\Models\Entry\EntryType;

trait HasEntryType
{
    public function types()
    {
        return $this->belongsToMany(EntryType::class, 'entry_type_entry_fieldsets');
    }

    public function hasEntryType($type)
    {
        if ($type instanceof EntryType) {
            return EntryType::firstWhere('id', $type->id) !== null;
        } elseif (is_string($type)) {
            return EntryType::firstWhere('name', $type) !== null;
        } elseif (is_int($type)) {
            return EntryType::firstWhere('id', $type) !== null;
        }

        return false;
    }
}