<?php

namespace Database\Seeders;

use App\Models\Entry\Entry;
use Illuminate\Database\Seeder;

class EntrySeeder extends Seeder
{
    public function run()
    {
        Entry::factory()
            ->count(5)
            ->with_deleted(25)
            ->with_type()
            ->with_fieldsets()
            ->create();
    }
}