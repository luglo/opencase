<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entry_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('title');
            $table->json('description')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->unique('name');
        });

        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->json('title');
            $table->json('description')->nullable();
            $table->unsignedBigInteger('entry_type_id')->nullable();

            $table
                ->foreign('entry_type_id')->nullable()
                ->references('id')->on('entry_types')
                ->onDelete('set null');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('entry_fieldsets', function (Blueprint $table) {
            $table->id();
            $table->json('label');
            $table->json('description')->nullable();
            $table->unsignedBigInteger('entry_id')->nullable();

            $table
                ->foreign('entry_id')->nullable()
                ->references('id')->on('entries')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('entry_type_entry_fieldsets', function (Blueprint $table) {
            $table->unsignedBigInteger('entry_type_id');
            $table->unsignedBigInteger('entry_fieldset_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entry_types');
        Schema::dropIfExists('entries');
        Schema::dropIfExists('entry_fieldsets');
    }
};
