<?php

namespace Database\Factories\FieldValue;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;
use MatanYadaev\EloquentSpatial\Objects\Point;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FieldValue\FieldValuePoint>
 */
class FieldValuePointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $en = $this->faker;
        $fr = FakerFactory::create('fr_FR');
        $de = FakerFactory::create('de_DE');

        return [
            'value' => new Point($this->faker->latitude(), $this->faker->longitude()),
            'description' => [
                'fr' => $fr->sentence(),
                'en' => $en->sentence(),
                'de' => $de->sentence(),
            ]
        ];
    }
}
