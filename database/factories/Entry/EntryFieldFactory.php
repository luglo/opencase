<?php

namespace Database\Factories\Entry;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

use App\Models\Entry\EntryField;
use App\Models\FieldValue\FieldValueDate;
use App\Models\FieldValue\FieldValueDatetime;
use App\Models\FieldValue\FieldValuePoint;
use App\Models\FieldValue\FieldValueNumber;
use App\Models\FieldValue\FieldValueText;
use App\Models\FieldValue\FieldValueTime;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Entry\EntryField>
 */
class EntryFieldFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $en = $this->faker;
        $fr = FakerFactory::create('fr_FR');
        $de = FakerFactory::create('de_DE');

        return [
            'label' => [
                'fr' => 'fr ' . $fr->name(),
                'en' => 'en ' . $en->name(),
                'de' => 'de ' . $de->name()
            ],
            'description' => [
                'fr' => $fr->sentence(),
                'en' => $en->sentence(),
                'de' => $de->sentence(),
            ]
        ];
    }

    public function with_random_value(array $types = null)
    {
        $allClasses = [
            FieldValueDate::class,
            FieldValueDatetime::class,
            FieldValuePoint::class,
            FieldValueNumber::class,
            FieldValueText::class,
            FieldValueTime::class,
        ];
        $classes = [];
        if ($types) {
            foreach ($types as $type) {
                if (in_array($type, $allClasses)) {
                    $classes[] = $type;
                }
            }
        } else {
            $classes = $allClasses;
        }
        
        if ($classes) {
            return $this->afterCreating(function (EntryField $field) use ($classes){
                $class =  $this->faker->randomElement($classes);
                $element = $class::factory()->create();
                $field->fieldvalueable()->associate($element)->save();
            });
        }
        return $this;
    }
}
