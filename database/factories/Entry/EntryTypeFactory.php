<?php

namespace Database\Factories\Entry;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Entry\EntryType>
 */
class EntryTypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $en = $this->faker;
        $fr = FakerFactory::create('fr_FR');
        $de = FakerFactory::create('de_DE');

        return [
            'name' => $en->unique()->name(),
            'title' => [
                'fr' => 'fr ' . $fr->name(),
                'en' => 'en ' . $en->name(),
                'de' => 'de ' . $de->name()
            ],
            'description' => [
                'fr' => $fr->sentence(),
                'en' => $en->sentence(),
                'de' => $de->sentence(),
            ]
        ];
    }
}
