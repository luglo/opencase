<?php

namespace Database\Factories\Entry;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Faker\Factory as FakerFactory;
use App\Models\Entry\Entry;
use App\Models\Entry\EntryType;
use App\Models\Entry\EntryFieldset;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Entry\Entry>
 */
class EntryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $en = $this->faker;
        $fr = FakerFactory::create('fr_FR');
        $de = FakerFactory::create('de_DE');

        return [
            'title' => [
                'fr' => $fr->name(),
                'en' => $en->name(),
                'de' => $de->name()
            ],
            'description' => [
                'fr' => $fr->sentence(),
                'en' => $en->sentence(),
                'de' => $de->sentence(),
            ]
        ];
    }

    public function with_deleted(int $chance = 25)
    {
        return $this->state(function (array $attributes) use ($chance){
            return [
                'deleted_at' => $this->faker->boolean($chance) ? Carbon::now() : null
            ];
        });
    }

    public function with_type()
    {
        return $this->state(function (array $attributes) {
            return [
                'entry_type_id' => EntryType::factory()->create()->id
            ];
        });
    }

    public function with_fieldsets()
    {
        return $this->afterCreating(function (Entry $entry) {
            $entry->fieldsets()->saveMany(
                EntryFieldset::factory()->count(rand(1, 5))
                    ->with_fields()->create());
        });
    }
}
