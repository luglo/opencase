<?php

namespace Database\Factories\Entry;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as FakerFactory;

use App\Models\Entry\EntryFieldset;
use App\Models\Entry\EntryField;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Entry\EntryFieldset>
 */
class EntryFieldsetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $en = $this->faker;
        $fr = FakerFactory::create('fr_FR');
        $de = FakerFactory::create('de_DE');

        return [
            'label' => [
                'fr' => 'fr ' . $fr->name(),
                'en' => 'en ' . $en->name(),
                'de' => 'de ' . $de->name()
            ],
            'description' => [
                'fr' => $fr->sentence(),
                'en' => $en->sentence(),
                'de' => $de->sentence(),
            ]
        ];
    }

    public function with_fields()
    {
        return $this->afterCreating(function (EntryFieldset $fieldset) {
            $fieldset->fields()->saveMany(
                EntryField::factory()->count(rand(1, 5))
                    ->with_random_value()
                    ->create()
                );
        });
    }
}
