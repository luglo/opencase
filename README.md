# OpenCase
A tool for real-time intervention and documentation in cases of human rights violations.

## Getting started
### Backend/GraphQL API
For the backend the [Laravel](https://laravel.com/) php framework is used. For development Laravel includes [Laravel Sail](https://laravel.com/docs/9.x/sail), a setup using Docker and Docker Compose. To get started, follow these steps:

1. Install Docker as described in [the official Docker docs](https://docs.docker.com/get-docker/)

2. If you're running Docker on Linux, follow the post-installation steps to [manage docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

3. Install Docker Compose as described in [the official Docker Compose docs](https://docs.docker.com/compose/install/)

4. Install the composer dependencies using this command:

```bash
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
```
5. Create the `.env` for the local development setup:

```bash
cp .env.local .env
```

6. As you will have to interact through the CLI with the Laravel application quite often, it's recommended to [setup a bash alias for `sail`](https://laravel.com/docs/9.x/sail#configuring-a-bash-alias)

7. Start up the development server with:

```bash
sail up -d

# or if you didn't setup the alias
./vendor/bin/sail up -d
```

This will start the local development server in the background and make the application available at http://localhost:8000. Have a look at [the official Laravel Sail docs](https://laravel.com/docs/9.x/sail) to see all possible commands and things you can do with it.

8. Set the application key:

```bash
sail artisan key:generate
```

9. Run the database migrations and seed the test data:

```bash
sail artisan migrate --seed
```

10. That's it! You can now explore the database using the defined GraphQL queries and mutations at http://localhost:8000/graphiql

### Frontend
For the Frontend a SPA using [Vue.js](https://vuejs.org/) is included at [resources/frontend](resources/frontend/). For the developers' convenience, there's a [package.json](package.json) in the project root which provides shortcuts to scripts defined in [resources/frontend/package.json](resources/frontend/package.json).

In order to start developing, follow these steps:

1. *Optional but recommended:* [Install nvm](https://github.com/nvm-sh/nvm#installing-and-updating) (Node Version Manager) and run:

```bash
nvm use
```

This will make nvm use the node version specified in [.nvmrc](.nvmrc) or show you the command to install it.

2. Install the npm dependencies:

```bash
npm run prepare
```

3. Run the development server:

```bash
npm run dev
```

4. That's it! You can now access the SPA development server at http://localhost:3000/.


## Additional Resources
- https://www.freecodecamp.org/news/build-graphql-server-with-laravel/
- https://github.com/starkovsky/laravel-vue-cli
- https://sebastiandedeyne.com/vite-with-laravel/
- https://github.com/Drakota/vue3-vite-apollo-example/
