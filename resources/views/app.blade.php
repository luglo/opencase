<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @php
        $manifest = json_decode(file_get_contents(public_path('build/manifest.json')), true);
    @endphp
    <script type="module" src="/build/{{ $manifest['index.html']['file'] }}"></script>
    <link rel="stylesheet" href="/build/{{ $manifest['index.html']['css'][0] }}">
  </head>
  <body>
    <div id="app"></div>
  </body>
</html>
