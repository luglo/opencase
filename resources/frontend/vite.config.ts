import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import liveReload from "vite-plugin-live-reload";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [
    vue(),
    liveReload(
      "../../(app|config|database|public|resources|routes|tests)/**/*.php"
    ),
  ],
  publicDir: "disable",
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  base: mode === "production" ? "/build/" : "/",
  build: {
    manifest: true,
    outDir: "../../public/build",
  },
  server: {
    port: 3000,
  },
}));
