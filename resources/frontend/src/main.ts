import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import ApolloClient from "apollo-boost";
import { DefaultApolloClient } from "@vue/apollo-composable";

const apolloClient = new ApolloClient({
  // You should use an absolute URL here
  uri: "http://localhost:8000/graphql",
});

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.provide(DefaultApolloClient, apolloClient);

app.mount("#app");
