# OpenCase Documentation

## Data Model
### Entries
```mermaid
erDiagram
    entries }o--|| entry_types : ""

    entry_fields }o--o{ entry_types : ""
    entry_fields }o--o{ entries : ""

    entry_fields ||--|{ entry_field_allowed_values : ""

    entry_field_changes }o--|| entry_fields : ""

    entry_field_value_numbers |o--o| entry_fields : "morphTo"
    entry_field_value_geolocations |o--o| entry_fields : "morphTo"
    entry_field_value_texts |o--o| entry_fields : "morphTo"
    entry_field_value_dates |o--o| entry_fields : "morphTo"
    entry_field_value_times |o--o| entry_fields : "morphTo"
    entry_field_value_datetimes |o--o| entry_fields : "morphTo"
    
    entry_field_value_selections |o--o{ entry_fields : ""
    entry_field_value_selections |o--o{ entry_field_value_numbers : ""
    entry_field_value_selections |o--o{ entry_field_value_geolocations : ""
    entry_field_value_selections |o--o{ entry_field_value_texts : ""
    entry_field_value_selections |o--o{ entry_field_value_dates : ""
    entry_field_value_selections |o--o{ entry_field_value_times : ""
    entry_field_value_selections |o--o{ entry_field_value_datetimes : ""

    entry_field_value_numbers |o--o{ entry_field_changes : ""
    entry_field_value_geolocations |o--o{ entry_field_changes : ""
    entry_field_value_texts |o--o{ entry_field_changes : ""
    entry_field_value_dates |o--o{ entry_field_changes : ""
    entry_field_value_times |o--o{ entry_field_changes : ""
    entry_field_value_datetimes |o--o{ entry_field_changes : ""
    entry_field_value_selections |o--o{ entry_field_changes : ""

    entry_fieldsets }o--o{ entry_types : ""
    entry_fieldsets }o--o{ entries : ""

    entry_fieldsets }o--|{ entry_fields : ""

    %% br
    entries {
        int id
        datetime created_at 
        int entry_type_id
        string title
        TRAIT translatable
    }
    entry_types {
        int id
        string title
        TRAIT translatable
    }
    entry_fields {
        int id
        string label
        text description
        int fieldvalueable_id
        string fieldvalueable_type
        TRAIT translatable
    }
    entry_field_allowed_values {
        string entry_field_value_type
        int entry_field_value_count "nullable"
    }
    entry_field_changes {
        datetime created_at
        int user_id
    }
    entry_field_value_numbers {
        int id
        float value "nullable"
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_geolocations {
        int id
        point value "nullable"
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_texts {
        int id
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_dates {
        int id
        date value "nullable"
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_times {
        int id
        time value "nullable"
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_datetimes {
        int id
        datetime value  "nullable"
        text description "nullable"
        TRAIT translatable
    }
    entry_field_value_selections {
        int id
        text description "nullable"
        TRAIT translatable
    }
    entry_fieldsets {
        int id
        string label
        text description
        TRAIT translatable
    }
```
